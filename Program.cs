﻿using Ass2Stating.HeroesInheritence;
using System;

namespace Ass2Stating
{
    class Program
    {
        static void Main(string[] args)
        {
            Hero chad = new Ranger("Chad");
            chad.GainXP(10);
            chad.ToString();
        }
    }
}
