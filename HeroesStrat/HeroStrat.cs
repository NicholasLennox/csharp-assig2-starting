﻿using Ass2Stating.HeroesInheritence;
using Ass2Stating.HeroesStrat.Strategy;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ass2Stating.HeroesStrat
{
    public class HeroStrat
    {
        public string Name { get; set; }

        // Stats
        public Stats BaseStats { get; set; }
        public Stats BonusStats { get; set; }

        public int Level { get; set; }

        public int CurrentXP { get; set; }

        public HeroType HeroType { get; set; }

        // Reference to hero strategy
        public IHeroStatStrategy HeroStrategy { get; set; }

        public HeroStrat(string name, HeroType heroType, IHeroStatStrategy heroStatStrategy)
        {
            Name = name;
            HeroType = heroType;
            Level = 1;
            CurrentXP = 0;
            //
            HeroStrategy = heroStatStrategy;
            BaseStats = HeroStrategy.GenerateBaseStats(new Stats());
        }

        public void GainXP(int xp)
        {
            // logic
            LevelUp();
        }

        private void LevelUp()
        {
            HeroStrategy.LevelUpStats(BaseStats);
        }

        public override string ToString()
        {
            // StringBuilder
            return BaseStats.Health.ToString();
        }
    }
}
