﻿using Ass2Stating.HeroesInheritence;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ass2Stating.HeroesStrat.Strategy
{
    public class RangerStatsStrategy : IHeroStatStrategy
    {
        public Stats GenerateBaseStats(Stats stats)
        {
            stats.Health = 100;
            //
            return stats;
        }

        public void LevelUpStats(Stats stats)
        {
            stats.Health += 5;
            //
        }
    }
}
