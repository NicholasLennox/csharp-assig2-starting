﻿using Ass2Stating.HeroesInheritence;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ass2Stating.HeroesStrat.Strategy
{
    public interface IHeroStatStrategy
    {
        Stats GenerateBaseStats(Stats stats);
        void LevelUpStats(Stats stats);
    }
}
