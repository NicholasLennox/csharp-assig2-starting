﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ass2Stating.HeroesInheritence
{
    public abstract class Hero
    {
        public string Name { get; set; }

        // Stats
        public Stats BaseStats { get; set; }
        public Stats BonusStats { get; set; }

        public int Level { get; set; }

        public int CurrentXP { get; set; }



        // Constructor

        public Hero(string name)
        {
            Name = name;
            Level = 1;
            CurrentXP = 0;
        }

        // Methods

        public void GainXP(int xp)
        {
            // logic
            LevelUp();
        }

        protected abstract void LevelUp();

        public override string ToString()
        {
            // StringBuilder
            return BaseStats.Health.ToString();
        }
    }
}
