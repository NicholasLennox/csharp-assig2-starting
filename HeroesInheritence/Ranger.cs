﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ass2Stating.HeroesInheritence
{
    public class Ranger : Hero
    {

        public Ranger(string name) : base(name)
        {
            // Set base stats
            BaseStats = new Stats()
            {
                Health = 100,
                Strength = 20,
                Dexterity = 10,
                Intelligence = 5
            };
        }

        protected override void LevelUp()
        {
            // Set extra stats after level up
            BaseStats.Health += 5;
        }
    }
}
